package View;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Dao.DaoCadastro;

import Modelo.Cadastro;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.util.ArrayList;

public class AlterarCadastroGUI extends JFrame {
	private JTextField txtNome;
	private JTextField txtTelefone;
	private JTextField txtEndereco;
	private JTextField txtSexo;
	private JTextField txtDataNasc;
	private JTextField txtCPF;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlterarCadastroGUI frame = new AlterarCadastroGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AlterarCadastroGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 507, 501);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(panel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("Nome");
		label.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		label.setBounds(10, 77, 59, 14);
		panel.add(label);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(10, 102, 414, 30);
		panel.add(txtNome);
		
		JLabel label_1 = new JLabel("CPF");
		label_1.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		label_1.setBounds(10, 11, 46, 14);
		panel.add(label_1);
		
		txtCPF = new JTextField();
		txtCPF.setColumns(10);
		txtCPF.setBounds(10, 36, 166, 30);
		panel.add(txtCPF);
		
		JLabel label_2 = new JLabel("Telefone");
		label_2.setFont(new Font("Segoe Script", Font.PLAIN, 11));
		label_2.setBounds(10, 152, 86, 14);
		panel.add(label_2);
		
		txtTelefone = new JTextField();
		txtTelefone.setColumns(10);
		txtTelefone.setBounds(10, 177, 137, 30);
		panel.add(txtTelefone);
		
		JLabel label_3 = new JLabel("Endereco");
		label_3.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		label_3.setBounds(10, 237, 86, 14);
		panel.add(label_3);
		
		txtEndereco = new JTextField();
		txtEndereco.setColumns(10);
		txtEndereco.setBounds(10, 262, 353, 30);
		panel.add(txtEndereco);
		
		JLabel label_4 = new JLabel("Sexo");
		label_4.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		label_4.setBounds(10, 303, 46, 14);
		panel.add(label_4);
		
		txtSexo = new JTextField();
		txtSexo.setColumns(10);
		txtSexo.setBounds(10, 328, 86, 30);
		panel.add(txtSexo);
		
		JLabel label_5 = new JLabel("Data de Nascimento");
		label_5.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		label_5.setBounds(10, 375, 121, 14);
		panel.add(label_5);
		
		txtDataNasc = new JTextField();
		txtDataNasc.setColumns(10);
		txtDataNasc.setBounds(10, 400, 218, 30);
		panel.add(txtDataNasc);
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				
				//Recupera da interface Gráfica
				String cpf = txtCPF.getText();
				String nome = txtNome.getText();
				String tel = txtTelefone.getText();
				String end = txtEndereco.getText();		
				String sexo = txtSexo.getText();
				String datanasc = txtDataNasc.getText();
				
				Cadastro f = new Cadastro();			
				f.setCPF(cpf);
				f.setNome(nome);
				f.setEndereco(end);
				f.setDatanasc(datanasc);
				f.setTelefone(tel);
				f.setSexo(sexo);
				
				
				DaoCadastro gravar = new DaoCadastro();
				
				gravar.altera(f);
				
				JOptionPane.showMessageDialog(null, "Alterado !");
				
				
			
			}
		});
		btnAlterar.setBounds(316, 400, 108, 27);
		panel.add(btnAlterar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				
				String cpf = txtCPF.getText();
				
				DaoCadastro c = new DaoCadastro();
				ArrayList <Cadastro> co = new ArrayList <Cadastro>();
				
				co = c.listaAlterar(cpf);
				int a=0;
				
				for (a=0; a<co.size();a++)
				{
					txtNome.setText (co.get(a).Nome);
					txtTelefone.setText (co.get(a).Telefone);
					txtEndereco.setText(co.get(a).Endereco);
					txtSexo.setText(co.get(a).Sexo);
					txtDataNasc.setText (co.get(a).Datanasc);
					
					
				}
			
				
			}
		});
		btnBuscar.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		btnBuscar.setBounds(231, 36, 89, 27);
		panel.add(btnBuscar);
	}
}
