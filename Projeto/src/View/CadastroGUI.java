package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Dao.DaoCadastro;
import Modelo.Cadastro;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class CadastroGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCPF;
	private JTextField txtTelefone;
	private JTextField txtEndereco;
	private JTextField txtSexo;
	private JTextField txtDatanasc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroGUI frame = new CadastroGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 482);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nome");
		lblNewLabel.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel.setBounds(10, 11, 59, 14);
		contentPane.add(lblNewLabel);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 36, 414, 30);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("CPF");
		lblNewLabel_1.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(10, 77, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(10, 102, 166, 30);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Telefone");
		lblNewLabel_2.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(10, 152, 86, 14);
		contentPane.add(lblNewLabel_2);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(10, 177, 137, 30);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Endereco");
		lblNewLabel_3.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel_3.setBounds(10, 237, 86, 14);
		contentPane.add(lblNewLabel_3);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(10, 262, 353, 30);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Sexo");
		lblNewLabel_4.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel_4.setBounds(10, 303, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		txtSexo = new JTextField();
		txtSexo.setBounds(10, 328, 86, 30);
		contentPane.add(txtSexo);
		txtSexo.setColumns(10);
		
		JLabel lblDataDeNascimento = new JLabel("Data de Nascimento");
		lblDataDeNascimento.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblDataDeNascimento.setBounds(10, 375, 121, 14);
		contentPane.add(lblDataDeNascimento);
		
		txtDatanasc = new JTextField();
		txtDatanasc.setBounds(10, 400, 218, 30);
		contentPane.add(txtDatanasc);
		txtDatanasc.setColumns(10);
		
		JButton btnNewButton = new JButton("Cadastrar");
		btnNewButton.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//Recupera da interface Gr�fica
				String Nome = txtNome.getText();
	            String CPF= txtCPF.getText();
				String Endereco= txtEndereco.getText();
				String Sexo  = txtSexo.getText();		
				String DataNasc  = txtDatanasc.getText();	
				String Telefone = txtTelefone.getText();
				
				
				Cadastro U = new Cadastro();			
				U.setNome(Nome);
				U.setCPF(CPF);
				U.setEndereco(Endereco);
				U.setSexo(Sexo);
				U.setDatanasc(DataNasc);
				U.setTelefone(Telefone);
				
				DaoCadastro gravar = new DaoCadastro();
				
				gravar.insere(U);
				
				JOptionPane.showMessageDialog(null, "Gravado");
				
				
			}
		});
		btnNewButton.setBounds(316, 400, 108, 27);
		contentPane.add(btnNewButton);
	}
}
