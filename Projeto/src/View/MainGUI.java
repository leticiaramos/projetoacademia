package View;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.ConFactory;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class MainGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI frame = new MainGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		mnCadastro.setForeground(Color.BLUE);
		mnCadastro.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		menuBar.add(mnCadastro);
		
		JMenuItem mntmFornecedor = new JMenuItem("Inserir");
		mntmFornecedor.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		mntmFornecedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CadastroGUI c = new CadastroGUI();
				c.setVisible(true);
				
				
				
			}
		});
		mnCadastro.add(mntmFornecedor);
		
		JMenuItem mntmAlterar = new JMenuItem("Alterar");
		mntmAlterar.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		mntmAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AlterarCadastroGUI a = new AlterarCadastroGUI();
				a.setVisible(true);
			}
		});
		mnCadastro.add(mntmAlterar);
		
		JMenu mnTreinos =
				new JMenu("Treinos");
		mnTreinos.setForeground(Color.BLUE);
		mnTreinos.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		menuBar.add(mnTreinos);
		
		JMenuItem mntmInserir = new JMenuItem("Inserir");
		mntmInserir.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		mntmInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				

				CadastroTreinosGUI c = new CadastroTreinosGUI();
				c.setVisible(true);
				
				
			}
		});
		mnTreinos.add(mntmInserir);
		
		JMenuItem mntmAlterar_1 = new JMenuItem("Alterar");
		mntmAlterar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AlteraTreinosGUI a = new AlteraTreinosGUI();
				a.setVisible(true);
			}
		});
		mntmAlterar_1.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		mnTreinos.add(mntmAlterar_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
