package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;

import Dao.DaoCadastro;
import Dao.DaoTreino;
import Modelo.Cadastro;
import Modelo.Treino;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JTextArea;


public class AlteraTreinosGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtCPF;
	private JTextField txtNome;
	private JCheckBox Musculacao;
	private JCheckBox Spinning;
	private JCheckBox ArtesMarciais;
	private JCheckBox EsteiraEBicicleta;
	private JCheckBox Zumba;
	private JCheckBox TreinoFuncional;
	private JButton btnAlterar;
	private JButton btnBuscar;
	private JTextField txtDatanasc;
	private JComboBox Frequencia;
	private JComboBox Classificacao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlteraTreinosGUI frame = new AlteraTreinosGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AlteraTreinosGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 497, 444);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoCliente = new JLabel("CPF do cliente");
		lblNomeDoCliente.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		lblNomeDoCliente.setBounds(10, 11, 93, 37);
		contentPane.add(lblNomeDoCliente);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(10, 44, 163, 27);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String cpf = txtCPF.getText();
				
				DaoCadastro c = new DaoCadastro();
				ArrayList <Cadastro> co = new ArrayList <Cadastro>();
				
				co = c.listaPorCPF(cpf);
				int a=0;
				
				for (a=0; a<co.size();a++)
				{
					txtNome.setText (co.get(a).Nome);
					txtDatanasc.setText (co.get(a).Datanasc);
				}
			}
		});
		btnBuscar.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		btnBuscar.setBounds(234, 44, 89, 27);
		contentPane.add(btnBuscar);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Segoe Print", Font.PLAIN, 12));
		lblNome.setBounds(10, 82, 93, 27);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 108, 270, 27);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblTreino = new JLabel("Treino");
		lblTreino.setForeground(Color.RED);
		lblTreino.setFont(new Font("Segoe Print", Font.BOLD, 12));
		lblTreino.setBounds(10, 146, 93, 27);
		contentPane.add(lblTreino);
		
		Frequencia = new JComboBox();
		Frequencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String escolha = (String) Frequencia.getSelectedItem();
				//JOptionPane.showMessageDialog(null, escolha);
			}
		});
		Frequencia.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		Frequencia.setModel(new DefaultComboBoxModel(new String[] {"  -- Frequ\u00EAncia --", "1x por semana", "2x por semana", "3x por semana", "4x por semana", "5x por semana ", "Todos os dias", "Definido pelo cliente"}));
		Frequencia.setToolTipText("");
		Frequencia.setBounds(10, 184, 127, 27);
		contentPane.add(Frequencia);
		
		Classificacao = new JComboBox();
		Classificacao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				

				String escolha = (String) Classificacao.getSelectedItem();
			//	JOptionPane.showMessageDialog(null, escolha);
			
			}
		});
		Classificacao.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		Classificacao.setModel(new DefaultComboBoxModel(new String[] {"-- Classifica\u00E7\u00E3o --", "Aer\u00F3bico", "Anaer\u00F3bico", "Aer\u00F3bico e anaer\u00F3bico"}));
		Classificacao.setBounds(220, 184, 137, 27);
		contentPane.add(Classificacao);
		
		Musculacao = new JCheckBox("Muscula\u00E7\u00E3o");
		Musculacao.setForeground(Color.BLUE);
		Musculacao.setFont(new Font("Segoe Print", Font.BOLD, 11));
		Musculacao.setBounds(10, 266, 97, 23);
		contentPane.add(Musculacao);
		
		JLabel lblAtividades = new JLabel("Atividades");
		lblAtividades.setForeground(Color.RED);
		lblAtividades.setFont(new Font("Segoe Print", Font.BOLD, 12));
		lblAtividades.setBounds(10, 231, 97, 23);
		contentPane.add(lblAtividades);
		
		EsteiraEBicicleta = new JCheckBox("Esteira e Bicicleta");
		EsteiraEBicicleta.setForeground(Color.BLUE);
		EsteiraEBicicleta.setFont(new Font("Segoe Print", Font.BOLD, 11));
		EsteiraEBicicleta.setBounds(10, 303, 127, 23);
		contentPane.add(EsteiraEBicicleta);
		
		Spinning = new JCheckBox("Spinning");
		Spinning.setForeground(Color.BLUE);
		Spinning.setFont(new Font("Segoe Print", Font.BOLD, 11));
		Spinning.setBounds(165, 266, 97, 23);
		contentPane.add(Spinning);
		
		Zumba = new JCheckBox("Zumba");
		Zumba.setForeground(Color.BLUE);
		Zumba.setFont(new Font("Segoe Print", Font.BOLD, 11));
		Zumba.setBounds(165, 303, 97, 23);
		contentPane.add(Zumba);
		
		ArtesMarciais = new JCheckBox("Artes Marciais");
		ArtesMarciais.setForeground(Color.BLUE);
		ArtesMarciais.setFont(new Font("Segoe Print", Font.BOLD, 11));
		ArtesMarciais.setBounds(305, 266, 114, 23);
		contentPane.add(ArtesMarciais);
		
		TreinoFuncional = new JCheckBox("Treino Funcional");
		TreinoFuncional.setForeground(Color.BLUE);
		TreinoFuncional.setFont(new Font("Segoe Print", Font.BOLD, 11));
		TreinoFuncional.setBounds(305, 303, 127, 23);
		contentPane.add(TreinoFuncional);
		
		btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				//Recupera da interface Gr�fica
				String Nome = txtNome.getText();
	            String CPF= txtCPF.getText();
	            String Datanasc = txtDatanasc.getText();
	            

	            String Freq = (String) Frequencia.getSelectedItem();
	            String Classific = (String) Classificacao.getSelectedItem();
		
	            int isMusculacao = Musculacao.isSelected() ? 1 : 0;
	            int isEsteiraEBicicleta = EsteiraEBicicleta.isSelected() ? 1 : 0;
	            int isSpinning = Spinning.isSelected() ? 1 : 0;
	            int isZumba =  Zumba.isSelected() ? 1 : 0;	
	            int isArtesMarciais = ArtesMarciais.isSelected() ? 1 : 0;
	            int isTreinoFuncional =  TreinoFuncional.isSelected() ? 1 : 0;	
	         
	        	
				
				
			Treino t = new Treino();
			
			t.setNome(Nome);
			t.setCPF(CPF);
			t.setDatanasc(Datanasc);
			t.setFrequencia(Freq);
			t.setClassificacao(Classific);
			t.setMusculacao(isMusculacao);
			t.setEsteiraEBicicleta(isEsteiraEBicicleta);
			t.setSpinning(isSpinning);
			t.setZumba (isZumba);
			t.setArtesMarciais (isArtesMarciais);
			t.setTreinoFuncional(isTreinoFuncional);
           
			
				
			DaoTreino gravar = new DaoTreino();
				
			gravar.altera(t);
				
				JOptionPane.showMessageDialog(null, "Gravado");

				
			}
		});
		btnAlterar.setFont(new Font("Segoe Print", Font.PLAIN, 13));
		btnAlterar.setBounds(173, 361, 97, 27);
		contentPane.add(btnAlterar);
		
		JLabel lblDataDeNascimento = new JLabel("Data de Nascimento");
		lblDataDeNascimento.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblDataDeNascimento.setBounds(305, 89, 127, 14);
		contentPane.add(lblDataDeNascimento);
		
		txtDatanasc = new JTextField();
		txtDatanasc.setColumns(10);
		txtDatanasc.setBounds(290, 108, 145, 27);
		contentPane.add(txtDatanasc);

	}
}
