package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Modelo.Cadastro;
import Modelo.Treino;

public class DaoTreino {
	
	// Configura essas variaveis de acordo com o seu banco
	private final String URL = "jdbc:mysql://localhost/academia",
			USER = "root", 
			PASSWORD = "leticia";

	private Connection con;
	private Statement comando;

	private void conectar() 
	{
		try {
			con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
			comando = con.createStatement();
			System.out.println("Conectado!");
			} 
		catch (ClassNotFoundException e) 
				{
					imprimeErro("Erro ao carregar o driver", e.getMessage());
				} 
		catch (SQLException e) 
					{
					imprimeErro("Erro ao conectar", e.getMessage());
					}
		}


	private void fechar() 
	{
		try {
				comando.close();
				con.close();
				System.out.println("Conex�o Fechada");
			} catch (SQLException e) 
				{
				imprimeErro("Erro ao fechar conex�o", e.getMessage());
				}
	}

	private void imprimeErro(String msg, String msgErro) {
		JOptionPane.showMessageDialog(null, msg, "Erro cr�tico", 0);
		System.err.println(msg);
		System.out.println(msgErro);
		System.exit(0);
	}

public void insere(Treino t){
	conectar();
	PreparedStatement insertTreino = null;
	String sql="";
	try {

		 sql = "INSERT INTO TREINOS VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		insertTreino = con.prepareStatement(sql);
		
 		insertTreino.setString(1, t.getNome());
 		insertTreino.setString(2, t.getCPF());
 		insertTreino.setString(3, t.getDatanasc());
 		insertTreino.setString(4, t.getFrequencia());
 		insertTreino.setString(5, t.getClassificacao());	
 		insertTreino.setInt(6, t.getMusculacao());
 		insertTreino.setInt(7, t.getEsteiraEBicicleta());
 		insertTreino.setInt(8, t.getSpinning());
 		insertTreino.setInt(9, t.getZumba());
 		insertTreino.setInt(10, t.getArtesMarciais());
 		insertTreino.setInt(11, t.getTreinoFuncional());
		
		int r =insertTreino.executeUpdate();

		if(r > 0){
			//comando.executeUpdate(sql);
			System.out.println("Inserida!");
			}
	} catch (SQLException e) {
		System.out.println(sql);
		imprimeErro("Erro ao inserir Treino", e.getMessage());
	} finally {
		fechar();
	}}


public void altera(Treino s ){
	conectar();
	PreparedStatement alterarTreino= null;
	
	try {
	
	String sql =  "UPDATE TREINOS SET "
			 + "nome = ?, cpf = ?, datanasc = ?,FREQUENCIA=?,CLASSIFICACAO=?,MUSCULACAO=?,ESTEIRA_BICICLETA=?,SPINNING=?,ZUMBA=?,ARTESMARCIAIS=?,TREINOFUNCIONAL=?"
			 + "WHERE CPF = " + s.getCPF();
	
	
	
	alterarTreino= con.prepareStatement(sql);
	
	
	alterarTreino.setString(1, s.getNome());
	alterarTreino.setString(2, s.getCPF());
	alterarTreino.setString(3, s.getDatanasc());
	alterarTreino.setString(4, s.getFrequencia());
	alterarTreino.setString(5, s.getClassificacao());
	alterarTreino.setInt(6, s.getMusculacao());
	alterarTreino.setInt(7, s.getEsteiraEBicicleta());
	alterarTreino.setInt(8, s.getSpinning());
	alterarTreino.setInt(9, s.getZumba());
	alterarTreino.setInt(10, s.getArtesMarciais());
	alterarTreino.setInt(11, s.getTreinoFuncional());
	
	
	

	//UPDATE treinos SET nome='Mauro', cpf = '222.222.222', datanasc = '1998-09-11 ', FREQUENCIA='Todos os dias',CLASSIFICACAO='Anaer�bico',MUSCULACAO=0,ESTEIRA_BICICLETA=0,SPINNING=1,ZUMBA=0,ARTESMARCIAIS=0,TREINOFUNCIONAL=0;
	//sql =  "UPDATE TREINOS "
	// + "nome = "+s.getNome()+", cpf = "+s.getCPF()+", datanasc = "+s.getDatanasc()+" , FREQUENCIA="+s.getFrequencia()+",CLASSIFICACAO="+s.getClassificacao()+",MUSCULACAO="+s.getMusculacao()+",ESTEIRA_BICICLETA=" +s.getEsteiraEBicicleta()+",SPINNING="+s.getSpinning()+",ZUMBA="+s.getZumba()+",ARTESMARCIAIS="+ s.getArtesMarciais()+",TREINOFUNCIONAL="+s.getTreinoFuncional()
	// + "WHERE CPF = " + s.getCPF();
	
	
	
	System.out.println(sql);
	int r=alterarTreino.executeUpdate();

	if(r > 0){
		//comando.executeUpdate(sql);
		System.out.println("Alterado!");
	}
	
	}catch(SQLException e){
		imprimeErro("Erro ao alterar Treino", e.getMessage());
	}
     finally {
	    fechar();
	    
     }
    } }


