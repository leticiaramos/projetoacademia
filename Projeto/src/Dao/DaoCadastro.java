package Dao;


import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JOptionPane;

import Modelo.Cadastro;
import Modelo.Treino;


public class DaoCadastro {


	// Configura essas variaveis de acordo com o seu banco
	private final String URL = "jdbc:mysql://localhost/academia",
			USER = "root", 
			PASSWORD = "leticia";

	private Connection con;
	private Statement comando;

	private void conectar() 
	{
		try {
			con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
			comando = con.createStatement();
			System.out.println("Conectado!");
			} 
		catch (ClassNotFoundException e) 
				{
					imprimeErro("Erro ao carregar o driver", e.getMessage());
				} 
		catch (SQLException e) 
					{
					imprimeErro("Erro ao conectar", e.getMessage());
					}
		}


	private void fechar() 
	{
		try {
				comando.close();
				con.close();
				System.out.println("Conex�o Fechada");
			} catch (SQLException e) 
				{
				imprimeErro("Erro ao fechar conex�o", e.getMessage());
				}
	}

	private void imprimeErro(String msg, String msgErro) {
		JOptionPane.showMessageDialog(null, msg, "Erro cr�tico", 0);
		System.err.println(msg);
		System.out.println(msgErro);
		System.exit(0);
	}

	public void insere(Cadastro u){
		conectar();
		PreparedStatement insertCadastro = null;
		String sql="";
		try {
	
			 sql = "INSERT INTO CLIENTE VALUES(?,?,?,?,?,?)";
			insertCadastro = con.prepareStatement(sql);
			insertCadastro.setString(2, u.getNome());
			insertCadastro.setString(1, u.getCPF());
			insertCadastro.setString(4, u.getTelefone());
	        insertCadastro.setString(3, u.getEndereco());
			insertCadastro.setString(5, u.getSexo());
     		insertCadastro.setString(6, u.getDatanasc());
		
			
			
			int r =insertCadastro.executeUpdate();

			if(r > 0){
				//comando.executeUpdate(sql);
				System.out.println("Inserida!");
				}
		} catch (SQLException e) {
			System.out.println(sql);
			imprimeErro("Erro ao inserir Aluno", e.getMessage());
		} finally {
			fechar();
		}
	}
	

	
	public void altera(Cadastro f){
		conectar();
		PreparedStatement alterarCadastro = null;
		
		try {
		
		String sql = "UPDATE CLIENTE "
				+ "SET NOME=?,TELEFONE=?,ENDERECO=?,SEXO=?,DATANASC=?"
				+ "WHERE CPF = ? ";
		
		alterarCadastro = con.prepareStatement(sql);
		
		alterarCadastro.setString(1, f.getNome());
		alterarCadastro.setString(2, f.getTelefone());
		alterarCadastro.setString(3, f.getEndereco());
		alterarCadastro.setString(4, f.getSexo());
		alterarCadastro.setString(5, f.getDatanasc());
		alterarCadastro.setString(6, f.getCPF());
		
		
		int r=alterarCadastro.executeUpdate();

		if(r > 0){
			//comando.executeUpdate(sql);
			System.out.println("Alterado!");
		}
		
		}catch(SQLException e){
			imprimeErro("Erro ao alterar Cliente", e.getMessage());
		}
	     finally {
		    fechar();}
	    }
		
	//m�todo para listar todos as linhas//
			public ArrayList<Cadastro> listaTodos(){
				conectar();
				ArrayList<Cadastro> listaCadastro
				      = new ArrayList<Cadastro>();
				try {
				
				PreparedStatement selecionarCadastro = null;
				ResultSet rs = null;
				
				String sql = "SELECT * FROM CLIENTE";
				
				selecionarCadastro = con.prepareStatement(sql);
				rs = selecionarCadastro.executeQuery(sql);
				
				while(rs.next()){
					Cadastro f = new Cadastro();
					f.setCPF(rs.getString("CPF"));
					f.setNome(rs.getString("NOME"));
					f.setEndereco(rs.getString("ENDERECO"));
					f.setTelefone(rs.getString("TELEFONE"));
					f.setDatanasc(rs.getString("DATANASC"));
					
					
					listaCadastro.add(f);
				}
				
				} catch(SQLException e){
					imprimeErro("Erro ao selecionar Cliente", 
							e.getMessage());
				}finally {
				    fechar();
			    }
				
				return listaCadastro;}
			
	
	
	 public ArrayList<Cadastro> listaPorCPF(String text) {
	// TODO Auto-generated method stub
		 
		conectar();
		
		ArrayList<Cadastro> listaCadastro 
		      = new ArrayList<Cadastro>();
		try {
		
		PreparedStatement selecionarCliente = null;
		ResultSet rs = null;
		
		String sql = "SELECT Nome,Datanasc FROM CLIENTE WHERE CPF LIKE '" + text + "%'";
		
		
		selecionarCliente = con.prepareStatement(sql);
		
		rs = selecionarCliente.executeQuery(sql);
		
		while(rs.next()){
			Cadastro f = new Cadastro();
			f.setNome(rs.getString("Nome"));
			f.setDatanasc(rs.getString("Datanasc"));
			
			
			listaCadastro.add(f);
		}
		
		} catch(SQLException e){
			imprimeErro("Erro ao selecionar Cliente", 
					e.getMessage());
		}finally {
		    fechar();
	    }
		
		return listaCadastro;

		
		
	}
	 
	 public ArrayList<Cadastro> listaAlterar(String text) {
		// TODO Auto-generated method stub
			conectar();
			ArrayList<Cadastro> listaCadastro 
			      = new ArrayList<Cadastro>();
			try {
			
			PreparedStatement selecionarCliente = null;
			ResultSet rs = null;
			
			String sql = "SELECT * FROM CLIENTE WHERE CPF LIKE '" + text + "%'";
			
			
			selecionarCliente = con.prepareStatement(sql);
			
			rs = selecionarCliente.executeQuery(sql);
			
			while(rs.next()){
				Cadastro f = new Cadastro();
				f.setNome(rs.getString("Nome"));
				f.setTelefone(rs.getString("Telefone"));
				f.setDatanasc(rs.getString("Datanasc"));
				f.setEndereco(rs.getString("Endereco"));
				f.setSexo(rs.getString("Sexo"));
			
				
				
				listaCadastro.add(f);
			}
			
			} catch(SQLException e){
				imprimeErro("Erro ao selecionar Cliente", 
						e.getMessage());
			}finally {
			    fechar();
		    }
			
			return listaCadastro;

			
			
		
}}